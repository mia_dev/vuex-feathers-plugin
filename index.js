export default (params) => {
    return (store) => {
      const owner_module = {state:{}, mutations:{}, namespaced:true}
      
      //
      Object.keys(params).forEach(
        (v) => {
          owner_module.state[v] = params[v]      
          if (Array.isArray(params[v])) {
            
            owner_module.mutations['up_'.concat(
              v
            )] = (state, data)  => {
              state[v] = data
            }

            owner_module.mutations['concat_'.concat(
              v
            )] = (state, data)  => {
              data.forEach(
                (av) => {
                  state[v].push(av)
                }
              )
            }

            owner_module.mutations['init_'.concat(
              v
            )] = (state)  => {
              state[v] = []
            }

          }

          if (typeof(params[v]) === 'object') {
            
            owner_module.mutations['up_'.concat(
              v
            )] = (state, data)  => {
              for (const key in data) {
                if (data.hasOwnProperty(key)) {
                  state[v][key] = data[key]    
                }
              }
              
            }
            
            owner_module.mutations['init_'.concat(
              v
            )] = (state)  => {
              state[v] = {}
            }
          }

          if (!Array.isArray(params[v]) &&  typeof(params[v]) !== 'object' ) {
            owner_module.mutations['up_'.concat(
              v
            )] = (state, data)  => {
              state[v] =  data
            }
            
            switch (typeof(params[v])) {
              case "boolean":
                owner_module.mutations['init_'.concat(
                  v
                )] = (state)  => {
                  state[v] = false
                }  
                break;

              case "number":
                owner_module.mutations['init_'.concat(
                  v
                )] = (state)  => {
                  state[v] = 0
                }  
                break;

              case "string":
                owner_module.mutations['init_'.concat(
                  v
                )] = (state)  => {
                  state[v] = ""
                }  
                break;
            
              default:
                break;
            }

            
            
          }
          
        }
      )
      //owner_module.state["entites"] = params["entites"]
      /*
      owner_module.mutations.initSchema = async (state) => {
        state.schema = {}
      }

      owner_module.mutations.initModel = async (state) => {
        state.model = {}
      }

      owner_module.mutations.initFindParams = async (state) => {
        state.find_params = {}
      }
      
      owner_module.mutations.initEntites = async (state) => {
        state.entites = []
      }
      */


      // commit state
      /*
      owner_module.mutations.upSchema = async (state, data) => {
        state.schema[data.key] = data.value

      }

      owner_module.mutations.upModel = async (state,data) => {
        state.model[data.key] = data.value
      }

      owner_module.mutations.upFindParams = async (state, data) => {
        state.find_params[data.key] = data.value
      }

      owner_module.mutations.upEntites = async (state, data) => {
        state.entites = data
      }

      
      owner_module.mutations.concatEntites = async (state, data) => {
        data.array.forEach(element => {
          state.entites.push(element)
        });
      }
      */

      // processing witch state

      owner_module.mutations.get = async (state, id) => {
        try {
          return await params.feathersClient.service(state.service).get(id)  
        } catch (error) {
          return false
        }
      }

      owner_module.mutations.upEntite = async (state, data) => {
        try {
          state.entite[data.key] = data.value  
          return true  
        } catch (error) {
          return false
        }
      }
  
      owner_module.mutations.put = async (state,id) => {
        try {
          if (id) {
            return await params.feathersClient.service(
              state.service
            ).patch(id,state.entite)    
          } else {
            return await params.feathersClient.service(
              state.service
            ).patch(state.entite[state.entite.id_field],state.entite)  
          }
          
        } catch (error) {
          return false
        }
      }

      owner_module.mutations.update = async (state,id) => {
        try {
          if (id) {
            return await params.feathersClient.service(
              state.service
            ).update(id,state.entite)    
          } else {
            return await params.feathersClient.service(
              state.service
            ).update(state.entite[state.entite.id_field],state.entite)  
          }
          
        } catch (error) {
          return false
        }
      }
  
      owner_module.mutations.remove = async (state, id) => {
        try {
          if (id) {
            await params.feathersClient.service(state.service).remove(id)
            return true  
          } else {
            await params.feathersClient.service(state.service).remove(
              state.entite[state.entite.id_field]
            )
            return true
          }
          
        } catch (error) {
          return false
        }
      }
  
      owner_module.mutations.create = async (state) => {
        try {
          await params.feathersClient.service(state.service).create(state.entite)
          return true
        } catch (error) {
          return false
        }
      }
  
      owner_module.mutations.find = async (state) => {
        try {
          //this.commit("initEntites")
          const res = await params.feathersClient.service(state.service).find(state.find_params)
          state.entites = res.data 
          state.skip = res.skip
          state.limit = res.limit
          state.total = res.total
          return true
        } catch (error) {
          return false
        }
      }
  
      owner_module.mutations.findConcat = async (state) => {
        try {
          const res = await params.feathersClient.service(state.service).find(find_params)
          res.data.array.forEach(element => {
            state.entites.push(element)
          });          
          state.skip = res.skip
          state.limit = res.limit
          state.total = state.entites.length
          return true
        } catch (error) {
          return false
        }
      }
  
      store.registerModule(params.service,owner_module)
    }
  }